import sys

from . import cli

if __name__ == '__main__':
    sys.argv[0] = __package__
    cli.main(sys.argv[1:])
