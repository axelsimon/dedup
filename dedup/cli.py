#!/usr/bin/python
import sys, textwrap, re, time
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import List, Optional

from dedup import __version__
from .core import status, SUPPORTED_FS, fs_type, set_opts
from .dedup import FileCollection, Deduper, DupPrinter
from .xioctl import FIEMAP_EXTENT_MERGED_CNT


class AllFormatter(ArgumentDefaultsHelpFormatter):
    """Help message formatter which retains formatting of all help text."""

    def _split_lines(self, text, width):
        """ keep original line breaks, but still wrap at width """
        res, last_indent = [], ''
        for m in re.findall(r'([^\n]*)(\n|$)(\s*)', text, re.MULTILINE):
            res.extend(textwrap.wrap(m[0], width, initial_indent=last_indent, subsequent_indent=m[2]))
            last_indent = m[2].replace('\t', '        ')
        if not res:
            res.extend(textwrap.wrap(text, width))
        return res

    def _fill_text(self, text, width, indent):
        """ for initial description only """
        return '\r\n'.join(self._split_lines(text, width))


def build_parser() -> ArgumentParser:
    desc = textwrap.dedent(f'''\
        Identical File Finder and Deduplication.
        
        Searches for identical files on large directory trees with multi millions of files consuming multi terabytes
        of data in an extremely efficient and fast way and by default also deduplicates those identical files via
        the Linux system function ioctl(FIDEDUPERANGE).
        
        NOTE: currently only a limited set of file systems support the Linux function `ioctl(FIDEDUPERANGE)`.
        Especially the widely used `ext4` filesystem does *not* support this deduplication.  
        Without a filesystem supporting *Deduplication* `dedup` can still be used as a still pretty fast 
        *"find identical files"* tool. See --print-only option.
        ''')
    parser = ArgumentParser(formatter_class=AllFormatter, allow_abbrev=False, description=desc)

    parser.add_argument('direcs', nargs='+', metavar='DIRECS',
                        help=textwrap.dedent("""\
                        directories and/or files to lookup/search for processing and optional deduplication.
                        For deduplication, all given files and directories must be located on the same 
                        filesystem. Mount points (to other filesystems) inside those directories are
                        silently skipped.
                        If not deduplicating (option '--print-only'), then different filesystems are accepted, 
                        but will reduce the overall lookup performance."""))
    parser.add_argument('--dup-src', '-D', nargs='+',
                        help=textwrap.dedent("""\
                        additional files and/or directories from where identical files are taken only as
                        source for deduplication, but no file from here gets deduplicated itself.
                        I.e.: all files here remain unchanged, but other files from DIRECS may deduplicate 
                        such that they will share the same content with files found here.
                        Files found here may be concurrently modified by other processes. And the same
                        filesystem  requirements apply as in DIRECS.
                        Usage examples:
                        1.) speed up deduplication if those directories were deduplicated earlier and it is
                            known that there were no changes meanwhile. So files found withing DUP_SRC will
                            not be read at all, if only other files within DUP_SRC could be matching
                            candidates.
                        2.) when using '--use-reflink' all files in DIRECS must not be modified by other
                            processes during the deduplication processing.
                            In order to still support deduplicating those files with other directories that
                            may be concurrently modified, those other directories can be added here, but only
                            as deduplication source."""))
    parser.add_argument('--verbose', '-v', default=False, action='store_true',
                        help="""more detailed output""")
    parser.add_argument('--quiet', '-q', default=False, action='store_true',
                        help="""don't even output status information about current running deduping progress.""")
    parser.add_argument('--version', '-V', action='version', version=f'dedup version {__version__}',
                        help="""output dedup version and exit""")
    parser.add_argument('--fail-always', '-F', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        exit with an error (2) even for recoverable errors like File-Not-Found or Permission
                        errors. By default, those errors are handled such that the involved files are skipped
                        and only a warning message is output."""))
    parser.add_argument('--multi-threaded', '-M', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        use several threads for deduplication. Deduplication is mainly disk IO bound.
                        But on modern disks which support command queuing, this may still deliver faster
                        results."""))
    parser.add_argument('--use-reflink', '-l', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        By default, deduplication is done via system function ioctl(FIDEDUPERANGE). This 
                        system function supplies fully transparent race free deduplication without any side
                        effects under any workload.
                        But as of Linux version 6.0 this is pretty slow. A faster approach which finally also
                        results in deduplicated files is by replacing identical files with new "reflink"
                        copied files (via ioctl_ficlone()).
                        This however can go wrong if other processes concurrently modify the files which get
                        replaced by the "reflink" copies. 
                        It is highly advised to use this only if it is ensured that all files in given DIRECS
                        do not get modified by other processes during the deduplication process.
                        """))
    parser.add_argument('--no-partial-dedup', '-N', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        The system function ioctl(FIDEDUPERANGE), checks if source and destination content is
                        identical before performing the actual deduplication. But deduplication, happens in
                        ranges, where each range of a file is checked and deduplicated separately.
                        To speed up deduplication, the implementation by default, does not always check if
                        files are identical within all ranges, and passes the final full equal check to this
                        system function.
                        This then may result in having only the start of two files deduplicated up to the
                        point where the files differ.
                        This option forces to always check ahead that two files are entirely identical before
                        performing the system function ioctl(FIDEDUPERANGE).
                        NOTE: there is still the possibility that between this check and the deduplication, 
                        another process modified one of the files, so that deduplication again will only
                        happen within equal ranges. I.e.: there still is no guarantee that only entire files
                        contents get deduplicated.
                        This does not apply when using: --use-reflink"""))
    parser.add_argument('--no-recursive', '-n', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        By default all given directories are searched recursive (nested).
                        This option will only lookup files in those given directories, ignoring any sub
                        directory."""))
    parser.add_argument('--start-small', '-S', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        By default deduplication starts with the large files. This option makes it start with
                        small files first."""))
    parser.add_argument('--size', '-s', default='', action='store', type=str,
                        help=textwrap.dedent("""\
                        check only files with the given size. Format: [[+]<min_size>][-<max_size>]
                        Where <min_size> and <max_size> can be suffixed with upper or lower cased: K, M, G
                        and T as their corresponding powers of 1024.
                        Examples:
                        -s +1K         - 1024 bytes or more
                        --size=-100m   - less than 100 mega bytes
                        -s 1000-1T     - 1000 bytes until at most one terrabyte.
                        -s 338-338     - only files with exactly 338 bytes 
                        Note: if the value starts with '-', then the long option passing with '=' must be
                        used, in order to prevent confusion with other dedup options.
                        """))
    parser.add_argument('--mtime', '-t', default='', action='store',
                        help=textwrap.dedent("""\
                        check only files with the given modification date. Min/max ranges work like in
                        '--size'.
                        The dates can be expressed in 's'=seconds, 'm'=minutes, 'h'=hours, 'd'=days suffixed
                        numbers as back from now, or by an ISO formated date/time. Also spaces are required
                        for the '-' between separator. So quoting is necessary. Examples:
                        -t "2019-03 - 23:00"         - from march 1st 2019 until 23:00 o'clock today
                        --mtime="-2022-12-12T08:00"  - all before december 12th 2022 at 8:00 o'clock
                        --mtime=-100d                - all modified before 100 days back from now
                        -t +100d                     - all modified since 100 days back from now"""))
    parser.add_argument('--match-re', '-m', default='', type=str,
                        help=textwrap.dedent("""\
                        REGEXP pattern which must match against the plain file basename (no path).
                        E.g.: "vm-[0-9].*\\.qcow2|test.img" matches file names starting with "vm-<digit>" and
                        ending with '.qcow2' or file 'test.img'"""))
    parser.add_argument('--print-only', '-p', default='', choices=['all', 'new', 'sh'],
                        help=textwrap.dedent("""\
                        Only print out identical files. One file per line and empty line between none
                        identical files. No changes will be actually made on the files. 
                        All file names are prefixed with one of:
                        '+' file to be deduped which has currently no shares with any other identical file.
                        '*' first file of a group of files sharing same space, but still needs to be 
                        re-deduped to another identical file ('~') having more shares.
                        '=' files sharing data with above '*' prefixed one, but still need to be re-deduped
                            to another file having more shares ('~').
                        '~' first file of a group of identical files, and picked as the source for
                            deduplication of all identical files. But no action is needed on this file.
                        '-' files already deduped to above '~' prefixed file. No action is needed on this
                            file.
                        To summarize: '+', '*' and '=' files need to be deduplicated, but disk space is only
                        saved for '+' and '*' files.
                        Options:
                        'all'   - prints out all of above prefixed files
                        'new'   - does not print out the '-' prefixed files and the '~' ones if no other not
                                  yet shared file is identical to it. I.e.: 'new' prints out only those files
                                  involved in pending deduplication.
                        'sh'    - print out the result as a shell script which can then be easily adjusted to
                                  perform any desired task on those identical files.
                        NOTE: files in DUP_SRC are only listed (and accounted) if they are picked as
                              deduplication sources.
                        """))
    parser.add_argument('--dry-run', '-d', default=False, action='store_true',
                        help=textwrap.dedent("""\
                        do not perform any deduplication, resulting in no filesystem modifications.
                        Just show what would be performed."""))
    parser.add_argument('--block-size', '-b', default=4*1024, type=int,
                        help=textwrap.dedent("""\
                        the block size used by the filesystem. Larger multiples of that size are also
                        allowed. The default should be good for most filesystems."""))
    return parser


def main(args: Optional[list] = None):
    start_tm = time.time()
    parser = build_parser()

    opts = parser.parse_args(args or sys.argv[1:])
    direcs: List[Path] = [Path(dir) for dir in opts.direcs]
    dups_idx = len(direcs)
    if opts.dup_src:
        direcs.extend([Path(dir) for dir in opts.dup_src])
    fstyp = fs_type(direcs[0])
    if not opts.print_only and fstyp not in SUPPORTED_FS:
        raise ValueError(
            f'Path "{direcs[0]}" is located on a "{fstyp}" filesystem, but only {",".join(SUPPORTED_FS)} are supported')
    dev = direcs[0].stat().st_dev
    unique_dirs: List[Path] = []
    for idx, dir in enumerate(direcs):
        if not dir.is_dir() and not dir.is_file():
            raise ValueError(f'Path "{dir}" is neither an existing file nor an existing directory')
        if idx > 0 and dir.stat().st_dev != dev and not opts.print_only:
            raise ValueError(f'Path "{dir}" is not on same filesystem as "{direcs[0]}". Can only dedupe within same FS.')
        unique = True
        for idx2, dir2 in enumerate(direcs):
            if idx == idx2:
                continue
            if dir == dir2 or (dir2.is_dir() and dir.is_relative_to(dir2)):
                print(f'Skipping given path "{dir}", because it is already included in "{dir2}"', file=sys.stderr)
                unique = False
                break
        if unique:
            unique_dirs.append(dir)
        elif idx < dups_idx:
            dups_idx -= 1

    set_opts(opts)
    if opts.multi_threaded:
        # Currently this seems to be always slower
        executor = ThreadPoolExecutor(min(len(unique_dirs), 4))
        futs = []
        for idx, dir in enumerate(unique_dirs):
            status(f'starting collecting: {dir}')
            files = FileCollection(dev, opts)
            futs.append(executor.submit(files.add_matching_path(dir), idx >= dups_idx))
        files = None
        for fut in futs:
            f = fut.result()
            if files is None:
                files = f
            else:
                files.merge_from(f)
    else:
        files = FileCollection(dev, opts)
        for idx, dir in enumerate(unique_dirs):
            status(f'starting collecting: {dir}')
            files.add_matching_path(dir, idx >= dups_idx)

    files.str_cache = None
    status(f'sorting {files.stat_match_files:,.0f} matched files by size. Total size: {files.stat_match_sz:,.0f}')
    files.sort(opts.start_small)
    status(f'collected total size {files.stat_match_sz:,.0f} from {files.stat_match_files:,.0f} files with {len(files.files):,.0f} different sizes')

    if opts.print_only:
        deduper = DupPrinter(opts)
    else:
        deduper = Deduper(opts)
    sz_cnt = 0
    for sz, lst in files.files.items():
        if isinstance(lst, list):
            sz_cnt += 1
            if sz_cnt % 1000 == 0 or len(lst) > 10000:
                status(f'currently processing at: {len(lst):,.0f} files with size {sz:,.0f}', status_tmp=False)
            deduper.process_same_size(lst, sz, files.multi_devs)
        # else: single ones are skipped

    deduped = 'Dedupeable' if opts.print_only else 'Deduped   '
    status('Done. Result:')
    status(f'Processed directories: {files.stat_match_dirs:17,.0f}')
    status(f'Processed files:       {files.stat_match_files:17,.0f}')
    status(f'Processed files size:  {files.stat_match_sz:17,.0f}')
    status(f'Not readable files:    {deduper.hash_grouper.stat_unreadable:17,.0f}')
    status(f'Merged extents:        {FIEMAP_EXTENT_MERGED_CNT:17,.0f}')
    status(f'Hashing IO count:      {deduper.hash_grouper.stat_io_cnt:17,.0f}')
    status(f'Full hashed files:     {deduper.hash_grouper.stat_full_file_cnt:17,.0f}')
    status(f'Total hashing size:    {deduper.hash_grouper.stat_sz:17,.0f}')
    status(f'Already deduped files: {deduper.stats_pre_duped_cnt:17,.0f}')
    status(f'Already deduped size:  {deduper.stats_pre_duped_sz:17,.0f}')
    status(f'{deduped} files:      {deduper.stats_new_duped_cnt:17,.0f}')
    status(f'{deduped} size:       {deduper.stats_new_duped_sz:17,.0f}')
    dur = time.time() - start_tm
    status(f'Total process time:             {time.strftime("%H:%M:%S", time.gmtime(dur))}')


if __name__ == '__main__':
    main(sys.argv[1:])
