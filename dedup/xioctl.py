import fcntl, struct, platform, ctypes, os, sys, tempfile
from fcntl import ioctl
from io import IOBase
from pathlib import Path
from typing import NamedTuple, List, Tuple, Union, Optional, Type


class _Ioctl(object):
    _IOC_NRBITS = 8
    _IOC_TYPEBITS = 8
    _IOC_SIZEBITS = 14
    _IOC_DIRBITS = 2

    _IOC_NONE = 0
    _IOC_WRITE = 1
    _IOC_READ = 2

    @staticmethod
    def _type_size(sz: Union[struct.Struct, int, Type[ctypes._SimpleCData]]) -> int:
        if isinstance(sz, struct.Struct):
            return sz.size
        elif isinstance(sz, int):
            return sz
        elif isinstance(sz, type) and issubclass(sz, ctypes._SimpleCData):
            return ctypes.sizeof(sz)
        else:
            raise TypeError(f'Invalid type for size: {sz}')

    @staticmethod
    def _request_type(request_type: Union[int, str]) -> int:
        if isinstance(request_type, int):
            return request_type
        if isinstance(request_type, str):
            if len(request_type) != 1:
                raise ValueError(f'Invalid length of request_type: "{request_type}"')
            return ord(request_type)
        else:
            raise ValueError(f'Invalid type of request_type: {request_type}')

    def _IOC(self, direction, request_type, request_nr, size):
        _IOC_NRSHIFT = 0
        _IOC_TYPESHIFT = _IOC_NRSHIFT + self._IOC_NRBITS
        _IOC_SIZESHIFT = _IOC_TYPESHIFT + self._IOC_TYPEBITS
        _IOC_DIRSHIFT = _IOC_SIZESHIFT + self._IOC_SIZEBITS
        return (
            (direction << _IOC_DIRSHIFT) |
            (request_type << _IOC_TYPESHIFT) |
            (request_nr << _IOC_NRSHIFT) |
            (size << _IOC_SIZESHIFT)
            )

    def IO(self, request_type, request_nr) -> int:
        return self._IOC(self._IOC_NONE, _Ioctl._request_type(request_type), request_nr, 0)

    def IOR(self, request_type, request_nr, size) -> int:
        return self._IOC(self._IOC_READ, _Ioctl._request_type(request_type), request_nr, _Ioctl._type_size(size))

    def IOW(self, request_type, request_nr, size) -> int:
        return self._IOC(self._IOC_WRITE, _Ioctl._request_type(request_type), request_nr, _Ioctl._type_size(size))

    def IOWR(self, request_type, request_nr, size) -> int:
        return self._IOC(self._IOC_READ | self._IOC_WRITE, _Ioctl._request_type(request_type), request_nr, _Ioctl._type_size(size))


class _IoctlAlpha(_Ioctl):
    _IOC_NRBITS = 8
    _IOC_TYPEBITS = 8
    _IOC_SIZEBITS = 13
    _IOC_DIRBITS = 3
    _IOC_NONE = 1
    _IOC_READ = 2
    _IOC_WRITE = 4


class _IoctlMips(_Ioctl):
    _IOC_SIZEBITS = 13
    _IOC_DIRBITS = 3
    _IOC_NONE = 1
    _IOC_READ = 2
    _IOC_WRITE = 4


class _IoctlParisc(_Ioctl):
    _IOC_NONE = 0
    _IOC_WRITE = 2
    _IOC_READ = 1


class _IoctlPowerPC(_Ioctl):
    _IOC_SIZEBITS = 13
    _IOC_DIRBITS = 3
    _IOC_NONE = 1
    _IOC_READ = 2
    _IOC_WRITE = 4


class _IoctlSparc(_Ioctl):
    _IOC_NRBITS = 8
    _IOC_TYPEBITS = 8
    _IOC_SIZEBITS = 13
    _IOC_DIRBITS = 3
    _IOC_NONE = 1
    _IOC_READ = 2
    _IOC_WRITE = 4


_MACHINE_IOCTL_MAP = {
    'alpha': _IoctlAlpha,
    'mips': _IoctlMips,
    'mips64': _IoctlMips,
    'parisc': _IoctlParisc,
    'parisc64': _IoctlParisc,
    'ppc': _IoctlPowerPC,
    'ppcle': _IoctlPowerPC,
    'ppc64': _IoctlPowerPC,
    'ppc64le': _IoctlPowerPC,
    'sparc': _IoctlSparc,
    'sparc64': _IoctlSparc,
}

Ioctl: _Ioctl = _MACHINE_IOCTL_MAP.get(platform.machine(), _Ioctl)()

# From linux/fiemap.h
FIEMAP_FLAG_SYNC = 0x0001
FIEMAP_FLAG_XATTR = 0x0002

FIEMAP_EXTENT_LAST = 0x0001
FIEMAP_EXTENT_UNKNOWN = 0x0002
FIEMAP_EXTENT_DELALLOC = 0x0004
FIEMAP_EXTENT_ENCODED = 0x0008
FIEMAP_EXTENT_DATA_ENCRYPTED = 0x0080
FIEMAP_EXTENT_NOT_ALIGNED = 0x0100
FIEMAP_EXTENT_DATA_INLINE = 0x0200
FIEMAP_EXTENT_DATA_TAIL = 0x0400
FIEMAP_EXTENT_UNWRITTEN = 0x0800
FIEMAP_EXTENT_MERGED = 0x1000
FIEMAP_EXTENT_SHARED = 0x2000

_FIEMAP_EXTEND_MAP = {
    0x0001: 'LAST',
    0x0002: 'UNKNOWN',
    0x0004: 'DEALLOC',
    0x0008: 'ENCODED',
    0x0080: 'DATA_ENCRYPTED',
    0x0100: 'NOT_ALIGNED',
    0x0200: 'DATA_INLINE',
    0x0400: 'DATA_TAIL',
    0x0800: 'UNWRITTEN',
    0x1000: 'MERGED',
    0x2000: 'SHARED'
}

# Derived from linux/fiemap.h
_FIEMAP_STRUCT = struct.Struct('=QQLLLL')  # sz: 20
_FIEMAP_EXTENT_STRUCT = struct.Struct('=QQQQQLLLL')  # sz: 38
# From linux/fs.h
_FS_IOC_FIEMAP = Ioctl.IOWR(ord('f'), 11, _FIEMAP_STRUCT)
FIEMAP_EXTENT_MERGED_CNT = 0


class FiemapExtent(NamedTuple):
    logical: int        # u64: always starting at zero, so can be derived
    physical: int       # u64
    length: int         # u64
    # reserved[2]: int  # u64
    flags: int          # u32
    # reserved[3]: int  # u32

    @property
    def is_good(self) -> bool:
        """currently verified flags resulting in equal content on same flags"""
        return self.flags & ~(FIEMAP_EXTENT_LAST+FIEMAP_EXTENT_DELALLOC+FIEMAP_EXTENT_SHARED) == 0

    @property
    def is_last(self) -> bool:
        return self.flags & FIEMAP_EXTENT_LAST == FIEMAP_EXTENT_LAST

    @property
    def is_dealloc(self) -> bool:
        return self.flags & FIEMAP_EXTENT_DELALLOC == FIEMAP_EXTENT_DELALLOC

    def is_next_physical(self, nxt: 'FiemapExtent') -> bool:
        """same flags and physical location continues without gap"""
        return nxt.physical == self.physical + self.length \
               and self.flags & ~FIEMAP_EXTENT_LAST == nxt.flags & ~FIEMAP_EXTENT_LAST

    def merged_with_nxt(self, nxt: 'FiemapExtent') -> 'FiemapExtent':
        # copying possibly FIEMAP_EXTENT_LAST
        return FiemapExtent(self.logical, self.physical, self.length + nxt.length, nxt.flags)

    def is_mergable(self, nxt: 'FiemapExtent') -> bool:
        # TODO: is physical always the same for: FIEMAP_EXTENT_DELALLOC
        # In other words: does it work to dedupe files with same dealloc size but different physical position
        return nxt.physical == self.physical + self.length \
               and nxt.logical == self.logical + self.length \
               and nxt.flags & ~FIEMAP_EXTENT_LAST == self.flags

    @property
    def flags_str(self):
        flags = []
        for k, f in _FIEMAP_EXTEND_MAP.items():
            if self.flags & ~k == k:
                flags.append(f)
        return '|'.join(flags)

    def __str__(self):
        return f'{self.physical}[{self.length}] off={self.logical} flags={self.flags_str}'


class Fiemap(NamedTuple):
    """Fiemap with empty extents is special: self.is_good == False"""
    # start: int              # u64: as passed in to ioctl_fiemap_all_extents()
    # length: int             # u64: as passed in to ioctl_fiemap_all_extents()
    # flags: int              # u32: as passed in to ioctl_fiemap_all_extents()
    # mapped_extents: int     # u32: returned number of extents
    # extent_count: int       # u32: memory allocated extents array size
    # reserved: int          # u32: reserved
    extents: Tuple[FiemapExtent, ...]  # tuple is hashable and less memory consuming

    @property
    def is_good(self) -> bool:
        """sanity check. If True, then various constraints are fulfilled like fiemap1 == fiemap2 => files are identical"""
        for e in self.extents:
            if not e.is_good:
                return False
        return self.extents and self.extents[-1].is_last

    @property
    def total_exts_size(self) -> int:
        sz = 0
        for e in self.extents:
            sz += e.length
        return sz

    @property
    def logical_size(self) -> int:
        if not self.extents:
            return 0
        lext = self.extents[-1]
        return lext.logical + lext.length

    @property
    def first_mergable_idx(self) -> int:
        exts = self.extents
        for idx in range(len(exts)-1):
            if exts[idx].is_mergable(exts[idx+1]):
                return idx
        return -1

    @staticmethod
    def merge_extents(extents: Union[List[FiemapExtent], Tuple[FiemapExtent, ...]]) -> Tuple[FiemapExtent, ...]:
        new_extents: Optional[List[FiemapExtent]] = None
        sz = len(extents)
        sz_1 = sz - 1
        idx = 0
        while idx < sz:
            nidx = idx
            while nidx < sz_1:
                if extents[nidx].is_mergable(extents[nidx+1]):
                    nidx += 1
                else:
                    break
            if nidx > idx:
                global FIEMAP_EXTENT_MERGED_CNT
                FIEMAP_EXTENT_MERGED_CNT += (nidx - idx)
                ext = extents[idx]
                length = extents[nidx].logical + extents[nidx].length - ext.logical
                ext = FiemapExtent(ext.logical, ext.physical, length, extents[nidx].flags)
                if new_extents is None:
                    new_extents = extents[0:idx]
                idx = nidx
                new_extents.append(ext)
            elif new_extents is not None:
                new_extents.append(extents[idx])
            idx += 1
        if new_extents is not None:
            return tuple(new_extents)
        elif isinstance(extents, tuple):
            return extents
        else:
            return tuple(extents)


def ioctl_fiemap_all_extents(fd: int, start=0, length=sys.maxsize, flags=0, merge_extents=True) -> Fiemap:
    """fetch all extents for given file.
        :param merge_extents: extents don't seem to be always merged, so merging them unifies files which still share same
               physical space but with different extent boundaries.
    """
    fiemap_data = _FIEMAP_STRUCT.pack(start, length, flags, 0, 0, 0)
    barray = bytearray(fiemap_data)
    ret = fcntl.ioctl(fd, _FS_IOC_FIEMAP, barray)
    if ret < 0:
        raise IOError('Calling ioctl(FS_IOC_FIEMAP, 0)')

    _fm_start, _fm_length, fm_flags, fm_mapped_extents, _fm_extent_count, _r1 = _FIEMAP_STRUCT.unpack_from(barray)
    count = fm_mapped_extents
    if count <= 0:
        if count < 0:
            raise IOError(f'Negative FIEMAP.fm_mapped_extents: {count}')
        return Fiemap(())  # Possibly empty file (by now)

    fiemap_data = _FIEMAP_STRUCT.pack(start, length, flags, 0, count, 0) \
                  + b'\0' * (_FIEMAP_EXTENT_STRUCT.size * count)

    barray = bytearray(fiemap_data)
    ret = fcntl.ioctl(fd, _FS_IOC_FIEMAP, barray)
    if ret < 0:
        raise IOError(f'Calling ioctl(FS_IOC_FIEMAP, {count})')

    _fm_start, _fm_length, fm_flags, fm_mapped_extents, _fm_extent_count, _r1 = _FIEMAP_STRUCT.unpack_from(barray)
    if fm_mapped_extents <= 0:
        if fm_mapped_extents < 0:
            raise IOError(f'Negative FIEMAP.fm_mapped_extents: {fm_mapped_extents}')
        return Fiemap(())  # Possibly empty file (by now)
    elif fm_mapped_extents > count:
        raise IOError(f'Received more extents ({fm_mapped_extents}) than requested {count} in ioctl(FS_IOC_FIEMAP)')

    extents: List[FiemapExtent] = []
    offset = _FIEMAP_STRUCT.size
    ext_sz = _FIEMAP_EXTENT_STRUCT.size
    for idx in range(fm_mapped_extents):
        fe_logical, fe_physical, fe_length, _r1, _r2, fe_flags, _r3, _r4, _r5 = \
            _FIEMAP_EXTENT_STRUCT.unpack_from(barray[offset:offset + ext_sz])

        # TODO: should fe_physical be set to same value for every FIEMAP_EXTENT_DELALLOC
        extents.append(FiemapExtent(fe_logical, fe_physical, fe_length, fe_flags))
        offset += ext_sz

    if merge_extents and len(extents) > 1:
        return Fiemap(Fiemap.merge_extents(extents))

    return Fiemap(tuple(extents))


FILE_DEDUPE_RANGE_SAME = 0
FILE_DEDUPE_RANGE_DIFFERS = 1

_FILE_DEDUPE_RANGE_STRUCT = struct.Struct('=QQHHI')
_FILE_DEDUPE_RANGE_INFO_STRUCT = struct.Struct('=qQQiI')
_FIDEDUPERANGE = Ioctl.IOWR(0x94, 54, _FILE_DEDUPE_RANGE_STRUCT.size)
#assert _FIDEDUPERANGE == 0xC0189436


class FileDedupeRange(NamedTuple):
    src_offset: int     # u64
    src_length: int     # u64
    dest_count: int     # u16
    reserved1: int      # u16
    reserved2: int      # u32
    # file_dedupe_range_info[0]

    def pack(self) -> bytes:
        return _FILE_DEDUPE_RANGE_STRUCT.pack(*self)

    @staticmethod
    def unpack_from(data: bytes, offset=0) -> 'FileDedupeRange':
        return FileDedupeRange(*_FILE_DEDUPE_RANGE_STRUCT.unpack_from(data, offset))


class FileDedupeRangeInfo(NamedTuple):
    dest_fd: int        # s64
    dest_offset: int    # u64
    bytes_deduped: int  # u64
    status: int         # s32
    reserved: int       # u32

    def pack(self) -> bytes:
        return _FILE_DEDUPE_RANGE_INFO_STRUCT.pack(*self)

    @staticmethod
    def unpack_from(data: bytes, offset=0) -> 'FileDedupeRangeInfo':
        return FileDedupeRangeInfo(*_FILE_DEDUPE_RANGE_INFO_STRUCT.unpack_from(data, offset))


def ioctl_dedup(fd_ro_src: int, rw_dst: Union[str, Path], sz: int) -> int:
    # NOTE: passing more than 1GB to FIDEDUPERANGE will only de-dupe the first one GB.
    # NOTE: file systems are only required to support 16MB blocks,
    #       so the linux kernel manages the size between 1GB and 16MB.
    #       So since loop implementation is anyway required, using 16MB always
    # max = 1024 * 1024 * 1024
    max = 1024 * 1024 * 16
    dst_fd = None
    try:
        dst_fd = os.open(rw_dst, os.O_RDWR)
        bytes_processed = 0
        while sz > 0:
            length = min(max, sz)

            fdr = FileDedupeRange(src_offset=bytes_processed, src_length=length, dest_count=1, reserved1=0, reserved2=0)
            fdri = FileDedupeRangeInfo(dest_fd=dst_fd, dest_offset=bytes_processed, bytes_deduped=0, status=FILE_DEDUPE_RANGE_SAME, reserved=0)

            buffer = bytearray(fdr.pack() + fdri.pack())
            ret = fcntl.ioctl(fd_ro_src, _FIDEDUPERANGE, buffer)
            if ret < 0:  # should not happen according to doc, but OSError is raised
                raise IOError(f'Failed to call ioctl(FIDEDUPERANGE, {rw_dst})')

            # fdr = FileDedupeRange.unpack_from(buffer)
            fdri = FileDedupeRangeInfo.unpack_from(buffer, _FILE_DEDUPE_RANGE_STRUCT.size)

            if fdri.status != FILE_DEDUPE_RANGE_SAME:
                if fdri.status == FILE_DEDUPE_RANGE_DIFFERS:
                    return bytes_processed
                else:
                    print(f'ioctl(FIDEDUPERANGE, {rw_dst}) returned error: {fdri.status}', file=sys.stderr)
                    return bytes_processed
            done = fdri.bytes_deduped
            assert done <= length
            bytes_processed += done
            if done <= 0:
                return bytes_processed  # don't repeat into endless loop

            sz -= done

        return bytes_processed
    finally:
        if dst_fd is not None:
            os.close(dst_fd)


_INT_SIZE = 4
# converted from: https://github.com/torvalds/linux/blob/master/tools/include/uapi/linux/fs.h
# and https://github.com/torvalds/linux/blob/master/arch/alpha/include/uapi/asm/ioctl.h
FICLONE = Ioctl.IOW(0x94, 9, _INT_SIZE)
#assert FICLONE == 0x40049409  # _IOC_WRITE = 1


def ioctl_ficlone(ro_src_fd: int, rw_dest: Union[int, str, Path, IOBase], keep_rw_dest_times=False):
    old_stats: Optional[os.stat_result] = None
    needs_close_dest = False
    rw_dest_fd = None
    try:
        old_stats = os.stat(rw_dest, follow_symlinks=False) if keep_rw_dest_times else None
        if isinstance(rw_dest, int):
            rw_dest_fd = rw_dest
        elif not isinstance(rw_dest, IOBase):
            rw_dest_fd = os.open(rw_dest, os.O_RDWR + os.O_CREAT)
            needs_close_dest = True
        else:
            rw_dest_fd = rw_dest.fileno()

        ret = ioctl(rw_dest_fd, FICLONE, ro_src_fd)
        if ret != 0:
            raise IOError(f'Failed to ioctl(FICLONE, {rw_dest} with: {ret}')
    finally:
        if needs_close_dest:
            os.close(rw_dest_fd)
        if old_stats:
            # Only need to update the times, access rights and ownership don't chamge by just opening file in 'w+b'
            os.utime(rw_dest, ns=(old_stats.st_mtime_ns, old_stats.st_mtime_ns))


def ioctl_dedup_ficlone(ro_src_fd: int, rw_dest: Union[str, Path], flock_dest=False) -> int:
    """Uses ioctl_ficlone for emulating dedupe, being as transparent and recoverable as possible.
        The destination file must not be modified during the operation.
        The implementation should be safe for any concurrent processes reading dest and/or source and
        is safe for concurrent processes modifying src, in which case the implementation detects it.
        If it is detected that any file was modified, then false is returned. Otherwise True.
        raises Exception on any IO error.
        :param flock_dest: additional security measure that other processes do not modify rw_dest during this
                           operation. But 'flock' is only advisory.
        :return:  positive size of cloned file or negative if modified and not cloned
    """
    if not isinstance(rw_dest, Path):
        rw_dest = Path(rw_dest)
    tmp_fd, tmp_file, rw_dst_fd, has_lock = None, None, None, False
    try:
        rw_dst_fd = os.open(str(rw_dest), os.O_RDWR)
        if flock_dest:
            fcntl.flock(rw_dst_fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        stats = os.stat(rw_dst_fd)
        tmp_fd, tmp_file = tempfile.mkstemp(prefix='.' + rw_dest.name + '-', suffix='.dedup-src.tmp', dir=rw_dest.parent)
        os.chmod(tmp_fd, stats.st_mode)
        os.chown(tmp_fd, stats.st_uid, stats.st_gid)

        ioctl_ficlone(ro_src_fd, tmp_fd)

        # check for same content in temp file and to be replaced dest
        sz = stats.st_size
        tmp_stats = os.stat(tmp_fd)
        if sz == tmp_stats.st_size:  # cached sz may have changed, but checked later again
            for tmp_data in iter(lambda: os.read(tmp_fd, 1024 * 16), b''):
                dst_data = os.read(rw_dst_fd, len(tmp_data))
                if dst_data != tmp_data:
                    return -1  # possible concurrent modification
            tmp_data = os.read(tmp_fd, 1024 * 4)
            if len(tmp_data) > 0:
                return -2  # file data was appended
        else:
            return -1  # possible concurrent modification

        ioctl_ficlone(tmp_fd, rw_dst_fd)
        # restore mtime, since modified by ficlone. ownership and access modes are kept
        os.utime(rw_dst_fd, ns=(stats.st_atime_ns, stats.st_mtime_ns))
        return sz
    finally:
        if tmp_fd is not None:
            os.close(tmp_fd)
        if tmp_file is not None:
            os.unlink(tmp_file)
        # fcntl.flock(rw_dst_fd, fcntl.LOCK_UN)  # automatically done on file close
        if rw_dst_fd is not None:
            os.close(rw_dst_fd)
