import os, hashlib, re, time, shlex, sys, textwrap, datetime
from argparse import Namespace
from pathlib import Path
from typing import List, Optional, Union, Dict, Callable, Iterable, Tuple

from .core import status, verbose, unreadable
from .xioctl import Fiemap, ioctl_dedup, ioctl_dedup_ficlone, ioctl_fiemap_all_extents


class DupSrcMixin:
    __slots__ = ()


class EntryName:
    __slots__ = ('path', 'name')
    path: Optional[str]
    name: str

    def __init__(self, name: str, path: Optional[str] = None):
        self.path = path
        self.name = name

    @staticmethod
    def create(name: str, path: Optional[str] = None, dup_src=False) -> 'EntryName':
        if dup_src:
            return EntryNameDS(name, path)
        else:
            return EntryName(name, path)

    @property
    def is_dup_src(self) -> bool:
        return isinstance(self, DupSrcMixin)

    def __str__(self):
        if self.path is None:
            return self.name
        else:
            return str(self.path) + os.sep + self.name


class EntryNameDS(EntryName, DupSrcMixin): ...


class EntryDups(EntryName):
    """name entry collecting other NameEntrys having same fiemap.
       If any NameEntry with same fiemap is found from 'dup_src', then that one is made the parent one.
       Because NameEntries sharing data with one 'dup_src' are not deduplicated to any other (better) one.
    """
    __slots__ = ('fiemap', 'dups')
    fiemap: Fiemap  # common for all dups. Currently only required for length od extents tuple
    dups: Union[EntryName, List[EntryName], None]   # those duplicates are hidden for most processing,
                                                    # but need to be considered once this instance needs to be deduped

    def __init__(self, name: str, path: str, fiemap: Fiemap, dups: Union[EntryName, List[EntryName], None] = None):
        super().__init__(name, path)
        self.fiemap = fiemap
        self.dups = dups

    @staticmethod
    def create(name: str, path: str, fiemap: Fiemap, dups: Union[EntryName, List[EntryName], None] = None, dup_src=False) -> 'EntryDups':
        if dup_src:
            return EntryDupsDS(name, path, fiemap, dups)
        else:
            return EntryDups(name, path, fiemap, dups)

    def add_dup(self, en: EntryName):
        if isinstance(self.dups, list):
            self.dups.append(en)
        elif self.dups is None:
            self.dups = en
        else:
            self.dups = [self.dups, en]

    @property
    def first_dup(self) -> Optional[EntryName]:
        if isinstance(self.dups, list):
            return self.dups[0]
        else:
            return self.dups

    @property
    def length(self) -> int:
        """length of dups + 1 (for self)"""
        if self.dups is None:
            return 1
        elif isinstance(self.dups, list):
            return len(self.dups) + 1
        else:
            return 2

    def _dup_src_last_key(self) -> int:
        """ sort dup_src ones last, and within same dup_src type, sort those with larger children count first,
         and within same dup_src type and same children count, take the one with least fiemap extents
        """
        ext_cnt = len(self.fiemap.extents)
        if isinstance(self, DupSrcMixin):
            return 2000000000 - self.length * 1000000 - ext_cnt
        else:
            return 1000000000 - self.length * 1000000 - ext_cnt

    @property
    def iter_dups(self) -> Iterable[EntryName]:
        """only dups, but not self"""
        if isinstance(self.dups, list):
            yield from self.dups
        elif self.dups is not None:
            yield self.dups

    @property
    def iter_all(self) -> Iterable[EntryName]:
        """self and dups"""
        yield self
        if isinstance(self.dups, list):
            yield from self.dups
        elif self.dups is not None:
            yield self.dups


class EntryDupsDS(EntryDups, DupSrcMixin): ...


class FileMatcher:
    __slots__ = ('root_dev', 'single_dev', 'multi_devs', 'min_sz', 'max_sz', 'min_mtm', 'max_mtm', 'name_re')
    root_dev: int       # first filesystem device found
    single_dev: bool    # option: requires unique/single filesystem device
    multi_devs: bool    # state: found multi filesystem devices
    min_sz: int
    max_sz: int
    min_mtm: float
    max_mtm: float
    name_re: Optional[re.Pattern]

    def __init__(self, dev: int, opts: Namespace):
        self.root_dev = dev
        self.single_dev = not opts.print_only
        self.multi_devs = False
        sz = opts.size
        if sz:
            self.min_sz, self.max_sz = self.parse_sz_limits(sz)
            verbose(f'Limitimg file size between {self.min_sz} and {self.max_sz}')
        else:
            self.min_sz, self.max_sz = 1, sys.maxsize
        self.name_re = re.compile(opts.match_re) if opts.match_re else None
        mtime = opts.mtime
        if mtime:
            self.min_mtm, self.max_mtm = self.parse_mtime_linits(mtime)
            fmt = '%Y-%m-%d %H:%M:%S'
            verbose(f'Limitimg mtime between {time.strftime(fmt, time.localtime(self.min_mtm))} and {time.strftime(fmt, time.localtime(self.max_mtm))}')
        else:
            self.min_mtm, self.max_mtm = 0, time.time() + 60 * 60 * 24 * 30

    @staticmethod
    def parse_sz_limits(sz: str) -> Tuple[int, int]:
        m = re.match(r'^(\+?)((\d+)([kmgtKMGT])?)?((-\d+)([kmgt])?)?$', sz)
        if not m or (not m.group(3) and not m.group(6)):
            raise ValueError(f'Incorrect format for given size range: "{sz}')
        units = {
            '': 1,
            'k': 1024,
            'm': 1024 * 1024,
            'g': 1024 * 1024 * 1024,
            't': 1024 * 1024 * 1024 * 1024,
        }
        mi, ma = 1, sys.maxsize
        if m.group(3):
            mi = int(m.group(3)) * units.get((m.group(4) or '').lower(), '')
        if m.group(5):
            ma = int(m.group(6)[1:]) * units.get((m.group(7) or '').lower(), '')
        return mi, ma

    @staticmethod
    def parse_date(dt: str) -> float:
        try:
            m = re.match(r'^(\d\d\d\d)(-(\d\d)(-(\d\d))?)?$', dt)
            if m:
                return datetime.datetime(year=int(m.group(1)), month=int(m.group(3) or 1), day=int(m.group(4) or 1)).timestamp()
            else:
                return datetime.datetime.fromisoformat(dt).timestamp()
        except ValueError or OverflowError as ex:
            try:
                ts = datetime.time.fromisoformat(dt)
                td = datetime.datetime.today()
                return td.replace(hour=ts.hour, minute=ts.minute, second=ts.second, microsecond=ts.microsecond).timestamp()
            except ValueError or OverflowError:
                units = {
                    's': 1,
                    'm': 60,
                    'h': 60 * 60,
                    'd': 60 * 60 * 24
                }
                if dt[-1].lower() in units.keys():
                    return time.time() - float(dt[:-1]) * units.get(dt[-1].lower(), 1)
                else:
                    return time.time() - float(dt)

    @staticmethod
    def parse_mtime_linits(mtime: str) -> Tuple[float, float]:
        m = re.match(r'^(\+)?([^-][^\s]*)?((\s+-\s*|-\s+|^-\s*)?(.*))$', mtime)
        if not m or (not m.group(2) and not m.group(3)):
            raise ValueError(f'Incorrect format for given time range: "{mtime}')
        mi, ma = 0, time.time() + 60 * 60 * 24 * 30
        if m.group(2):
            mi = FileMatcher.parse_date(m.group(2))
        if m.group(5):
            ma = FileMatcher.parse_date(m.group(5))
        return mi, ma


class FileCollection(FileMatcher):
    """file collection grouped by file size"""
    __slots__ = ('recursive', 'files', 'str_cache',
                 'stat_not_matching', 'stat_match_files', 'stat_match_sz', 'stat_match_dirs', 'stat_dirs_failed')

    recursive: bool
    files: Dict[int, Union[EntryName, List[EntryName]]]
    str_cache: Dict[str, str]

    stat_not_matching: int
    stat_match_files: int
    stat_match_sz: int
    stat_match_dirs: int
    stat_dirs_failed: int

    def __init__(self, dev: int, opts: Namespace):
        super().__init__(dev, opts)
        self.recursive = not opts.no_recursive
        self.files = {}
        self.str_cache = {}

        self.stat_not_matching = 0
        self.stat_match_files = 0
        self.stat_match_sz = 0
        self.stat_match_dirs = 0
        self.stat_dirs_failed = 0  # mainly due to permission denied

    def add_matching_path(self, file: Path, dup_src=False) -> 'FileCollection':
        if file.is_file():
            stat = file.stat()
            sz = stat.st_size
            if sz < self.min_sz or sz > self.max_sz:
                self.stat_not_matching += 1
                return self
            mtime = stat.st_mtime
            if mtime < self.min_mtm or mtime > self.max_mtm:
                self.stat_not_matching += 1
                return self
            name = file.name
            if self.name_re and not self.name_re.match(name):
                self.stat_not_matching += 1
                return self
            if self.root_dev != stat.st_dev:
                if self.single_dev:
                    self.stat_not_matching += 1
                    return self
                else:
                    self.multi_devs = True
            self.stat_match_files += 1
            self.stat_match_sz += sz
            lst = self.files.get(sz, None)
            cname = self.str_cache.get(name, None)
            if cname is None:
                self.str_cache[name] = cname = name
            if lst is None:
                self.files[sz] = EntryName.create(cname, str(file.parent), dup_src)
            elif isinstance(lst, list):
                lst.append(EntryName.create(cname, str(file.parent), dup_src))
            else:
                self.files[sz] = [lst, EntryName.create(cname, str(file.parent), dup_src)]
            if self.stat_match_files % 100000 == 0:
                status(f'currently collected {self.stat_match_files:,.0f} files', True)
        elif file.is_dir():
            self.scan_direc(str(file), dup_src)
        return self

    def scan_direc(self, parent: str, dup_src=False) -> 'FileCollection':
        try:
            with os.scandir(parent) as it:
                self.stat_match_dirs += 1
                for entry in it:
                    ent: os.DirEntry = entry
                    if ent.is_file(follow_symlinks=False):
                        stat = ent.stat(follow_symlinks=False)
                        sz = stat.st_size
                        if sz < self.min_sz or sz > self.max_sz:
                            self.stat_not_matching += 1
                            continue
                        mtime = stat.st_mtime
                        if mtime < self.min_mtm or mtime > self.max_mtm:
                            self.stat_not_matching += 1
                            continue
                        name = ent.name
                        if self.name_re and not self.name_re.match(name):
                            self.stat_not_matching += 1
                            continue
                        if self.root_dev != stat.st_dev:
                            if self.single_dev:
                                self.stat_not_matching += 1
                                continue
                            else:
                                self.multi_devs = True
                        self.stat_match_files += 1
                        self.stat_match_sz += sz
                        lst = self.files.get(sz, None)
                        cname = self.str_cache.get(name, None)
                        if cname is None:
                            self.str_cache[name] = cname = name
                        if lst is None:
                            self.files[sz] = EntryName.create(cname, parent, dup_src)
                        elif isinstance(lst, list):
                            lst.append(EntryName.create(cname, parent, dup_src))
                        else:
                            self.files[sz] = [lst, EntryName.create(cname, parent, dup_src)]
                        if self.stat_match_files % 100000 == 0:
                            status(f'currently collected {self.stat_match_files:,.0f} files', True)
                    elif ent.is_dir(follow_symlinks=False):
                        if not self.recursive or (self.single_dev and self.root_dev != ent.stat().st_dev):
                            continue
                        paren = parent + os.sep + ent.name
                        self.scan_direc(paren, dup_src)
        except PermissionError as ex:
            self.stat_dirs_failed += 1
            unreadable(ex)
        return self

    def merge_from(self, other: 'FileCollection'):
        """Note: inplace merge, destroing files in 'other'"""
        other_files = other.files
        self_files = self.files
        for sz, ents in self_files.items():
            oent = other_files.get(sz)
            if isinstance(oent, list):
                if isinstance(ents, list):
                    ents.extend(oent)
                else:  # isinstance(ents, EntryName):
                    oent.append(ents)
                    self_files[sz] = oent
            elif oent is not None:
                if isinstance(ents, list):
                    ents.append(oent)
                else:  # isinstance(ents, EntryName):
                    self_files[sz] = [ents, oent]
        for sz, ents in other_files.items():
            if sz not in self_files:
                self_files[sz] = ents
        self.stat_match_sz += other.stat_match_sz
        self.stat_match_dirs += other.stat_match_dirs
        self.stat_dirs_failed += other.stat_dirs_failed
        self.stat_match_files += other.stat_match_files
        self.stat_not_matching += other.stat_not_matching

    def sort(self, small_first=False):
        self.files = dict(sorted(self.files.items(), key=lambda item: item[0], reverse=not small_first))


class BlockHasher:
    __slots__ = ('min_sz', 'blk_def')
    min_sz: int
    blk_def: Callable[[int], Tuple[int, int]]

    def __init__(self, min_sz: int, blk_def: Callable[[int], Tuple[int, int]]):
        self.min_sz = min_sz
        self.blk_def = blk_def

    def hash_block(self, entry: EntryName, file_sz: int) -> Optional[bytes]:
        offset, blksz = self.blk_def(file_sz)
        hl = hashlib.sha256()
        try:
            with open(str(entry), "rb") as fp:
                fp.seek(offset)
                data = fp.read(blksz)  # first 2 blocks
                hl.update(data)
                return hl.digest()
        except PermissionError as ex:
            unreadable(ex)
            return None


class FileHasher(BlockHasher):
    __slots__ = ('chunk_sz', )
    chunk_sz: int

    def __init__(self, min_sz: int, blk_def: Callable[[int], Tuple[int, int]], chunk_sz: int):
        super().__init__(min_sz, blk_def)
        self.chunk_sz = chunk_sz

    def hash_block(self, entry: EntryName, file_sz: int) -> Optional[bytes]:
        with open(str(entry), "rb") as fp:
            hl = None
            for data in iter(lambda: fp.read(self.chunk_sz), b''):
                if hl is None:
                    # data may have changed, so both checks required. Allowing exactly 16 (the size of the hash) is also
                    # critical for attacks with file content == hash of other content
                    if file_sz < 16 and len(data) < 16:
                        return data
                    hl = hashlib.sha256()
                hl.update(data)
            # Possibly empty file:
            return hl.digest() if hl is not None else b''


class HashGrouper:
    """Hashes and groups files by same hash"""
    __slots__ = ('block_sz', 'chunk_sz', 'allow_pairs', 'block_hashers',
                 'stat_io_cnt', 'stat_full_file_cnt', 'stat_sz', 'stat_unreadable')
    block_sz: int
    chunk_sz: int
    allow_pairs: bool
    block_hashers: List[BlockHasher]

    stat_io_cnt: int
    stat_full_file_cnt: int
    stat_sz: int
    stat_unreadable: int

    def __init__(self, opts: Namespace):
        self.block_sz = opts.block_size
        self.chunk_sz = 32 * self.block_sz  # e.g. 128K
        self.allow_pairs = not opts.no_partial_dedup and not opts.print_only
        self.block_hashers = [
            # first block
            BlockHasher(16 * 1024, lambda sz: (0, self.block_sz)),
            # last full block plus rest
            BlockHasher(64 * 1024, lambda sz: ((sz - self.block_sz) & ~(self.block_sz - 1), self.block_sz)),
            # middle block
            BlockHasher(256 * 1024, lambda sz: (int(sz / 2) & ~(self.block_sz - 1), self.block_sz)),
            # 1/4 block
            BlockHasher(1024 * 1024 * 1024, lambda sz: (int(sz / 4) & ~(self.block_sz - 1), self.block_sz)),
            # 3/4 block
            BlockHasher(16 * 1024 * 1024, lambda sz: (int(sz / 4 * 3) & ~(self.block_sz - 1), self.block_sz)),
            # block at 1MB. Good for VM images
            BlockHasher(256 * 1024 * 1024, lambda sz: (1024 * 1024, self.block_sz * 2)),
            # Full hasher
            FileHasher(0, lambda sz: (0, sz), self.chunk_sz)
        ]
        self.stat_io_cnt = 0
        self.stat_full_file_cnt = 0
        self.stat_sz = 0
        self.stat_unreadable = 0

    def group_hashed(self, entries: Iterable[EntryDups], sz: int, level: int = 0) -> Iterable[Union[EntryDups, List[EntryDups]]]:
        """group given files all already having same size, such, that:
         1.) group contains only single a blank EntryDup item
         2.) group is a list of two items and self.allow_pairs
         3.) group is a list of identical files, checked by secure hash of full content
        """
        hasher = self.block_hashers[level]
        if sz < hasher.min_sz:
            hasher = self.block_hashers[-1]
        by_hash: Dict[bytes, Union[EntryDups, List[EntryDups]]] = {}
        needs_more_hashing = False
        missing_dups_src = 0 if isinstance(hasher, FileHasher) else 1  # entries in hash not having a dup_src elem
        for ent in entries:  # sorted by dup_src last
            if not missing_dups_src and ent.is_dup_src:
                # every entry in hash has one (already the best) dup_src elem too. So can not get better
                break
            self.stat_io_cnt += 1
            self.stat_sz += hasher.blk_def(sz)[1]
            if isinstance(hasher, FileHasher):
                self.stat_full_file_cnt += 1
                large = sz > 100 * 1024 * 1024  # * 1024
                if large:
                    status(f'currently hashing file with {sz:,.0f} bytes in: {str(ent)}', status_tmp=False)
                elif self.stat_io_cnt % 1000 == 0:
                    status(
                        f'currently hashed chunks: {self.stat_io_cnt:,.0f} with total size {self.stat_sz:,.0f} at file size {sz:,.0f}',
                        status_tmp=True)
            elif self.stat_io_cnt % 1000 == 0:
                status(
                    f'currently hashed chunks: {self.stat_io_cnt:,.0f} with total size {self.stat_sz:,.0f} at file size {sz:,.0f}',
                    status_tmp=True)
            h = hasher.hash_block(ent, sz)
            if h is None:
                self.stat_unreadable += 1
                continue  # could not read
            cur = by_hash.get(h, None)
            if cur is None:
                if not ent.is_dup_src:
                    by_hash[h] = ent
                    missing_dups_src += 1
                # else only dup_src types left, and those don't get deduped here anyway
            elif isinstance(cur, list):
                if ent.is_dup_src:
                    if not cur[-1].is_dup_src:
                        missing_dups_src -= 1
                        needs_more_hashing = True
                        cur.append(ent)
                    elif not isinstance(hasher, FileHasher):
                        # assert needs_more_hashing
                        cur.append(ent)  # having one dup_src, but more precise hashing may split again
                    # else: found already best one to share with, so no need to add more
                else:
                    needs_more_hashing = True
                    cur.append(ent)
            else:
                if ent.is_dup_src:
                    assert not cur.is_dup_src
                    missing_dups_src -= 1
                needs_more_hashing |= not self.allow_pairs
                by_hash[h] = [cur, ent]
        if not needs_more_hashing or level < 0 or isinstance(hasher, FileHasher):
            yield from by_hash.values()
            return
        level += 1
        for ent in by_hash.values():
            if isinstance(ent, list):
                if self.allow_pairs and len(ent) == 2:
                    yield ent
                else:
                    yield from self.group_hashed(ent, sz, level)
            else:
                yield ent


class Deduper:
    __slots__ = ('hash_grouper', 'use_reflink', 'dry_run', 'no_partial_dedup', 'blksz',
                 'print_only',
                 'stats_pre_duped_cnt', 'stats_pre_duped_sz', 'stats_new_duped_cnt', 'stats_new_duped_sz',
                 'stats_read_errors')
    hash_grouper: HashGrouper
    use_reflink: bool
    dry_run: bool
    no_partial_dedup: bool
    blksz: int
    print_only: str

    stats_pre_duped_cnt: int
    stats_pre_duped_sz: int

    stats_new_duped_cnt: int
    stats_new_duped_sz: int

    stats_read_errors: int

    def __init__(self, opts: Namespace):
        self.hash_grouper = HashGrouper(opts)
        self.use_reflink = opts.use_reflink
        self.dry_run = opts.dry_run
        self.no_partial_dedup = opts.no_partial_dedup
        self.blksz = opts.block_size
        self.print_only = opts.print_only

        self.stats_pre_duped_cnt = 0
        self.stats_pre_duped_sz = 0
        self.stats_new_duped_cnt = 0
        self.stats_new_duped_sz = 0
        self.stats_read_errors = 0

    def group_by_fiemap(self, entries: List[EntryName], sz: int) -> List[EntryDups]:
        """group given same size entries already sorted by dup_src-is-last by fiemap. """
        by_fiemap: Dict[Fiemap, EntryDups] = {}
        bad_fiemaps: List[EntryDups] = []
        for ent in entries:  # sorted by dup_src last
            fd = None
            try:
                # with open(str(ent), 'rb') as fd:
                fd = os.open(str(ent), os.O_RDONLY)
                fiemap = ioctl_fiemap_all_extents(fd, merge_extents=True)
                if not fiemap.is_good:
                    # May need further analyze. Right now, two same such ones are not considered identical/shared
                    verbose(f'Can not interpret FIEMAP of {str(ent)}: {fiemap}')
                    # Use empty fake Fiemap so that len(extents) is zero. I.e.: don't prefer as dup source.
                    if not ent.is_dup_src:
                        bad_fiemaps.append(EntryDups.create(ent.name, ent.path, Fiemap(()), None, ent.is_dup_src))
                    continue
                same = by_fiemap.get(fiemap, None)
                if same is None:
                    by_fiemap[fiemap] = EntryDups.create(ent.name, ent.path, fiemap, None, ent.is_dup_src)
                else:
                    same.add_dup(ent)
                    self.stats_pre_duped_cnt += 1
                    self.stats_pre_duped_sz += sz
            except PermissionError or FileNotFoundError as ex:
                self.stats_read_errors += 1
                unreadable(ex)
                # ignoring
            finally:
                if fd is not None:
                    os.close(fd)
        res = list(by_fiemap.values())
        res.extend(bad_fiemaps)
        return res

    def best_dedupe_source(self, same_ents: List[EntryDups]) -> EntryDups:
        """ take that entry with the most already shared other ents and if having the same number of shares,
        then take the entry which is least fragmented. (small number of fiemap.extents)
        Given same_ents is sorted by EntryName._dup_src_last_key with at most one dup_src element.
        So either first or last one.
        """
        ent1 = same_ents[0]
        ent2 = same_ents[-1]
        if not ent2.is_dup_src:
            return ent1
        cnt1 = ent1.length
        cnt2 = ent2.length
        if cnt1 > cnt2:
            return ent1
        elif cnt2 > cnt1:
            return ent2
        if len(ent1.fiemap.extents) >= len(ent2.fiemap.extents):
            return ent1
        else:
            return ent2

    def process_same_content(self, entries: List[EntryDups], sz: int):
        """deduplicate identical files where being identical was checked ahead"""
        assert len(entries) > 0 and not entries[0].is_dup_src  # still sorted by src_dup last
        src_ent = self.best_dedupe_source(entries)
        src_fd = None
        # even with two entries, it may still be possible to do nothing: if best is first and second is already dup_src
        try:
            for ent in entries:
                if ent == src_ent:
                    continue
                if ent.is_dup_src:
                    return  # sorted by dup_src last. Children not being dup_src are still ignored
                failed = 0
                for dup in ent.iter_all:
                    if dup.is_dup_src:
                        break  # never for first (parent) one, see above
                    if src_fd is None:
                        status(f'DEDUP: {str(src_ent)} [sz={sz:,.0f}, dups={src_ent.length - 1}]')
                        src_fd = os.open(str(src_ent), os.O_RDONLY)
                    status(f'    {"   " if dup != ent else ""}<= {str(dup)}', end='')  # indent children
                    try:
                        if self.dry_run:
                            deduped = sz
                        elif self.use_reflink:
                            deduped = ioctl_dedup_ficlone(src_fd, str(dup))
                        else:
                            deduped = ioctl_dedup(src_fd, str(dup), sz)
                        if deduped == sz:
                            status(' OK', notime=True)
                        elif deduped > 0:
                            # NOTE: just a good guess, both files may have been modified, deduped fine, but have now different size
                            status(f' PARTLY={deduped:,.0f}', notime=True)
                            failed += 1
                            # NOTE: not accounted at all, because the file may have had the
                            # share at start of file already before deduping.
                            # Because checking was only done by match-all-extents else it differs
                        else:  # deduped <= 0:
                            status(' DIFFERS', notime=True)
                            failed += 1
                            # Not changed at all. If this is also first one, then assume that other shares do as well
                            if dup == ent:
                                break
                    except PermissionError as ex:
                        status(' FAILED', notime=True)
                        self.stats_read_errors += 1
                        failed += 1
                        unreadable(ex)
                if not failed:  # Only saving space for one instance, since athers where duped already
                    self.stats_new_duped_cnt += 1
                    self.stats_new_duped_sz += sz
        except IOError as ex:
            unreadable(ex)
        finally:
            if src_fd is not None:
                os.close(src_fd)

    def process_same_size(self, entries: List[EntryName], sz: int, multi_devs: bool):
        """dedupe files all having same size"""
        if len(entries) <= 1:
            return
        if multi_devs:  # Fiemaps are only unique within same dev
            empty_fiemap = Fiemap(())
            ents = [EntryDups.create(e.name, e.path, empty_fiemap, None, e.is_dup_src) for e in entries]
        else:
            ents: List[EntryDups] = self.group_by_fiemap(entries, sz)
        if self.print_only:
            if not ents:
                return
        else:
            if len(ents) <= 1:
                return

        ents = sorted(ents, key=EntryDups._dup_src_last_key)
        if ents[0].is_dup_src:
            return

        for ent in self.hash_grouper.group_hashed(ents, sz):
            if not isinstance(ent, list):  # length == 1
                if not self.print_only or not ent.dups:
                    continue
                self.process_same_content([ent], sz)  # print_only
            else:
                self.process_same_content(ent, sz)


class DupPrinter(Deduper):

    def __init__(self, opts: Namespace):
        super().__init__(opts)
        if self.print_only == 'sh':
            print(textwrap.dedent(f"""\
            #!/bin/sh
            # Generated by dedup on: {time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())}
            # Command line: {" ".join(sys.argv)}
                        
            # change below, depending on the task requested. 
            # The default decodes the file prefixes and prints out similar as would be done with: "--print-only all"
            do_identicals() {{
                [ $# -lt 2 ] && echo "Error: need at least two arguments" >&2 && exit 2
                local main="" sub_main="" arg file typ
                for arg in "$@"; do
                    file="${{arg#?}}"
                    case "$arg" in
                        [~]?*) typ='main' && main="$file";; # picked as deduplication source
                        [-]?*) typ='share' ;;               # already shared with deduplication source
                        [+]?*) typ='single' ;;              # identical without any other shares
                        [*]?*) typ='sub-main' && sub_main="$file" ;;  # identical and having other shares 
                                                            # but not shared with 'main'
                        [=]?*) typ='sub-share' ;;           # shared with previous 'sub-main' but not with 'main'
                        *)     echo "Error: unknown file name prefix: '$arg'" >&2 && exit 2 ;;
                    esac
                    printf "%s: %s\\n" "$typ" "$file"
                done 
                echo  # empty line separator
            }}
            
            if [ -f '~/.config/dedup/dedup_shell_functions' ]; then
                # Alternative custom overwrites of above 'do_identicals'
                . '~/.config/dedup/dedup_shell_functions'
            fi
            
            """))

    def process_same_content(self, entries: List[EntryDups], sz: int):
        if self.print_only == 'new' and len(entries) <= 1:
            return
        lns = []
        src_ent = self.best_dedupe_source(entries)
        lns.append('~' + src_ent.path + os.sep + src_ent.name)
        if not src_ent.is_dup_src and self.print_only != 'new':
            for ent in src_ent.iter_dups:
                lns.append('-' + ent.path + os.sep + ent.name)

        for ent in entries:
            if ent == src_ent:
                continue
            if ent.dups is None:
                lns.append('+' + ent.path + os.sep + ent.name)
                self.stats_new_duped_cnt += 1
                self.stats_new_duped_sz += sz
            else:
                lns.append('*' + ent.path + os.sep + ent.name)
                self.stats_new_duped_cnt += 1
                self.stats_new_duped_sz += sz
                for e in ent.iter_dups:
                    lns.append('=' + e.path + os.sep + e.name)
                    self.stats_new_duped_cnt += 1
        all = ''
        try:
            if self.print_only == 'sh':
                all = 'do_identicals ' + " ".join([shlex.quote(f) for f in lns])
            else:
                all = '\n'.join(lns)
            print('\n' + all)
        except UnicodeEncodeError as ex:
            ln = ''
            try:
                for ln in all.splitlines(keepends=False):
                    print(ln)
            except UnicodeEncodeError as ex1:
                print(f'Error file: {ln.encode("utf-8", errors="backslashreplace").decode(encoding="ASCII")}', file=sys.stderr)
                unreadable(ex1)
        print('')
