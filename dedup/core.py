import time, sys
from argparse import Namespace
from pathlib import Path
from typing import Union, Optional

SUPPORTED_FS = ('xfs', 'btrfs')
LAST_STATUS_TEMP = False
QUIET = False
VERBOSE = False
FAIL_ALWAYS = False


def set_opts(opts: Namespace):
    global QUIET, VERBOSE, FAIL_ALWAYS
    QUIET = opts.quiet
    VERBOSE = opts.verbose
    FAIL_ALWAYS = opts.fail_always


def status(msg: str, status_tmp=False, end='\n', notime=False):
    if QUIET:
        return
    global LAST_STATUS_TEMP
    tm = '' if notime else time.strftime('%H:%M:%S', time.localtime())
    try:
        if status_tmp and LAST_STATUS_TEMP:
            print(f'\r{tm} {msg}', end='')
        elif LAST_STATUS_TEMP:
            print(f'\n{tm} {msg}', end=end)
        elif status_tmp:
            print(f'{tm} {msg}', end='')
        else:
            print(f'{tm} {msg}', end=end)
    except UnicodeEncodeError as ex:
        print(f'Error line: {msg.encode("utf-8", errors="backslashreplace").decode(encoding="ASCII")}', file=sys.stderr)
        print(f'{ex}: {msg.encode("utf-8", errors="backslashreplace").decode(encoding="ASCII")}', file=sys.stderr)
    LAST_STATUS_TEMP = status_tmp


def verbose(msg: str):
    if VERBOSE:
        print(msg)


def unreadable(ex: Exception):
    try:
        print(ex, file=sys.stderr)
    except UnicodeEncodeError as ex:
        print(f'{ex}: {str(ex).encode("utf-8", errors="backslashreplace").decode(encoding="ASCII")}', file=sys.stderr)
    if FAIL_ALWAYS:
        sys.exit(2)


def fs_type(dir: Union[str, Path]) -> Optional[str]:
    dir = Path(dir).resolve()
    with open('/proc/mounts', 'r', encoding='utf-8') as fp:
        # OUTPUT: device mount_point filesystem flags ...
        lines = [ln.split(' ') for ln in fp.readlines()]
        sorted_lines = sorted(lines, key=lambda ln: len(ln[1]), reverse=True)
        for ln in sorted_lines:
            if dir.is_relative_to(ln[1]):
                return ln[2]
    return None

