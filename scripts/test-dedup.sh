#!/bin/sh
set -e

CP="/bin/cp"

fail() {
    echo "$*" >&2
    exit 1
}

_ex() {
    echo "$*" >&2
    "$@"
}

append_file() {
    local file="$1" data i
    shift
    for data in "$@"; do
        for i in $(seq 1 1024); do
            echo -n "$data" >>"$file"
        done
    done
}

check_mods_eq() {
    local f1="$1"
    local f2="$2"
    local s1 s2
    s1="$(stat -c '%A %G:%U %s %y' "$f1")"
    s2="$(stat -c '%A %G:%U %s %y' "$f2")"

    if [ "$s1" != "$s2" ]; then
        fail "Files differ in access-modes/times/ownership: $f1 ($s1) vs $2 ($s2)"
    fi
    return 0
}

check_content_eq() {
    local f1="$1"
    local f2="$2"
    if ! diff -b -q "$f1" "$f2"; then
        fail "Files differ in content: $f1 vs. $f2"
    fi
    return 0
}


file_shares_xfs() {
    local file="$1"
    xfs_bmap "$file" | tail -n +2
}

file_shares_btrfs() {
    # todo:
    echo "any val"
}

CUR_FS="xfs"

file_shares() {
    file_shares_"$CUR_FS" "$@"
}

check_shares_eq() {
    local f1="$1"
    local f2="$2"
    local s1 s2
    s1="$(file_shares "$f1")"
    s2="$(file_shares "$f2")"
    if [ "$1" != "$2" ]; then
        fail "Files differ in shares: $f1\n$s1\nvs. $f2\n$s2"
    fi
    return 0
}

check_dirs_eq() {
    local dir1="$(readlink -f "$1")"
    local dir2="$(readlink -f "$2")"
    local file f2
    for file in $(find "$dir1" -type f)
    do
        f2="$dir2${file#$dir1}"
        check_mods_eq "$file" "$f2"
        check_content_eq "$file" "$f2"
    done
}

cp() {
    "$CP" "$@"
}

reflink() {
    "$CP" "--reflink=always" "$@"
}

check_out_contains() {
    local not=""
    [ "$1" = "not" ] && not="not" && shift
    local out="$1"
    local regex="$2"
    
    if echo "$out" | grep -qEe "$regex"; then
        [ -n "$not" ] && fail "Did find '$regex' in '$out': $(echo "$out" | grep -qEe "$regex")"
    else
        if [ -n "$3" ]; then
            if ! echo "$out" | grep -qEe "$3"; then
                fail "Did not find '$regex' or '$3' in '$out'"
            fi
        else
            [ -z "$not" ] && fail "Did not find '$regex' in '$out'"
        fi
    fi
    return 0
}

test_dedup_dir() {
    local out shar
    mkdir data

    append_file data/FileA "AAAA" "BBBB"
    cp data/FileA data/FileB
    reflink data/FileB data/FileC
    cp data/FileA data/FileD
    chmod a+rwx data/FileD
    append_file data/FileE "XXXX" "YYYY"
    cp -ap data back

    out="$(_ex dedup data/ --print-only all)"

    check_dirs_eq data back
    check_out_contains "$out" "^[+]data/FileA"
    check_out_contains "$out" "^[~-]data/FileB"
    check_out_contains "$out" "^[~-]data/FileC"
    check_out_contains "$out" "^[+]data/FileD"
    check_out_contains not "$out" "^.data/FileE"

    _ex dedup data/ "$@"

    check_dirs_eq data back
    if [ "$CUR_FS" = "xfs" ]; then
        shar="$(file_shares data/FileB)"
        [ "$shar" = "$(file_shares data/FileA)" ] || fail "File data/FileA not shared"
        [ "$shar" = "$(file_shares data/FileC)" ] || fail "File data/FileC not shared"
        [ "$shar" = "$(file_shares data/FileD)" ] || fail "File data/FileD not shared"
        [ "$shar" = "$(file_shares data/FileE)" ] && fail "File data/FileE shared: $shar"
    fi

    out="$(_ex dedup data/ --print-only all --quiet)"

    check_out_contains "$out" "^[~-]data/FileA"
    check_out_contains "$out" "^[~-]data/FileB"
    check_out_contains "$out" "^[~-]data/FileC"
    check_out_contains "$out" "^[~-]data/FileD"
    check_out_contains not "$out" "^.data/FileE"

    out="$(_ex dedup data/ --print-only new --quiet)"

    check_out_contains not "$out" "^.data/File[A-E]"

    rm -r data/ back/
}

test_dup_src() {
    mkdir data dups
    # AAAA BBBB
    # FileA <-> FileC
    # FileB <-> FileD
    # FileE
    # AAAA XXXX
    # FileF
    append_file data/FileA "AAAA" "BBBB"
    cp data/FileA data/FileB
    reflink data/FileA data/FileC
    reflink data/FileB data/FileD
    cp data/FileA data/FileE
    append_file data/FileF "AAAA" "XXXX"  # single dup

    # AAAA BBBB
    # data:FileE <-> FileG <-> FileH  # most links, so one should be source
    # FileI  # should remain untouched
    # AAAA XXXX
    # FileJ <-> FileK  # one is source
    # AAAA YYYY
    # File
    reflink data/FileE dups/FileG
    reflink data/FileE dups/FileH
    cp data/FileA dups/FileI
    cp data/FileF dups/FileJ
    reflink dups/FileJ dups/FileK
    append_file data/FileL "AAAA" "YYYY"

    cp -ap data data_back
    cp -ap dups dups_back

    local out sG sI sJ
    sG="$(file_shares dups/FileG)"
    sI="$(file_shares dups/FileI)"
    sJ="$(file_shares dups/FileJ)"

    out="$(_ex dedup data --dup-src dups --print-only all --quiet)"

    check_out_contains "$out" "^[*=]data/FileA"
    check_out_contains "$out" "^[*=]data/FileB"
    check_out_contains "$out" "^[*=]data/FileC"
    check_out_contains "$out" "^[*=]data/FileD"
    check_out_contains "$out" "^[~-]data/FileE"
    check_out_contains "$out" "^[+]data/FileF"
    check_out_contains "$out" "^[~-]dups/FileG"
    check_out_contains "$out" "^[~-]dups/FileH"
    check_out_contains "$out" "^[+]dups/FileI"  # only in print
    check_out_contains "$out" "^[~-]dups/FileJ" "^[~-]dups/FileK"
    check_out_contains not "$out" "^.dups/FileL"

    _ex dedup data/ --dup-src dups/ "$@"

    check_dirs_eq data data_back
    check_dirs_eq dups dups_back

    if [ "$CUR_FS" = "xfs" ]; then
        [ "$sG" = "$(file_shares data/FileA)" ] || fail "data/FileA not shared with dups/FileG"
        [ "$sG" = "$(file_shares data/FileB)" ] || fail "data/FileB not shared with dups/FileG"
        [ "$sG" = "$(file_shares data/FileC)" ] || fail "data/FileC not shared with dups/FileG"
        [ "$sG" = "$(file_shares data/FileE)" ] || fail "data/FileE not shared with dups/FileG"
        [ "$sG" = "$(file_shares dups/FileH)" ] || fail "dups/FileE not shared with dups/FileH"

        [ "$sI" = "$(file_shares dups/FileI)" ] || fail "dups/FileI was changed in dups"

        [ "$sJ" = "$(file_shares data/FileF)" ] || fail "data/FileF not shared with dups/FileJ"
    fi

    rm -r data dups data_back dups_back
}

run_fs_tests() {
    local dir="$1"

    _ex test_dedup_dir
    _ex test_dedup_dir --use-reflink
    _ex test_dup_src
    _ex test_dup_src --use-reflink
}

test_fs() {
    local name="$1"
    local type="${2:-xfs}"
    local size="${3:-80M}"  # 40M is too less for btrfs creation
    local loop_dev image mnt
    local cur_dir

    image="$(readlink -f "$name")-$type.img"
    mnt="$(readlink -f "./tst_mnt.$$")"

    # creating file ahead with "real" content before calling truncate seems to be necessary
    # in order to prevent losetup error: failed to set up loop device: No such file or directory
    printf "\x00\x00\x00\x00\x00\x00\x00\x00" >"$image"
    _ex truncate -s "$size" "$image"
    loop_dev="$(_ex losetup -f --show "$image")"
    _ex "mkfs.$type" -L "deduptst$type" "$loop_dev"
    _ex mkdir "$mnt"
    _ex mount -t "$type" "$loop_dev" "$mnt"

    CUR_FS="$type"
    cur_dir="$(pwd)"
    _ex cd "$mnt"
    _ex run_fs_tests "$mnt"
    _ex cd "$cur_dir"

    _ex umount "$mnt"
    _ex rmdir "$mnt"
    _ex losetup -d "$loop_dev"
    _ex rm "$image"
}

test_all_fs() {
    _ex test_fs test-image xfs
    _ex test_fs test-image btrfs
}

if [ $# -eq 0 ]; then
    cat <<EOF >&2
    usage: $0 DIREC
    runs some dedup tests by creating XFS and BTRFS images in DIREC,
    mounts them, creates files, runs dedup and tests the result.
    RETURN 0 if all tests succeed
EOF
    exit 1
fi

_ex cd "$1"
test_all_fs

