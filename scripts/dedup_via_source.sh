#!/bin/sh
# starts dedup from source distribution without having to install this package
SCRIPT="$(readlink -f "$0")"
SCRIPT_DIR="${SCRIPT%/*}"

PYTHON="$(which python3 2>/dev/null)"
PYTHON="${PYTHON:-"$(which python 2>/dev/null)"}"

PYTHONPATH="$SCRIPT_DIR/..:$PYTHONPATH" exec "$PYTHON" -m dedup "$@"
