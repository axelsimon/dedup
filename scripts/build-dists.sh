#!/bin/sh
set -e

IMAGE="dedup"
SCRIPT="$(readlink -f "$0")"
SCRIPT_DIR="${SCRIPT%/*}"

if [ "$1" = "--force-build" ] || ! docker image inspect "$IMAGE" 2>/dev/null 1>&2 ; then
    cd "$SCRIPT_DIR/.."
    docker build -f docker/Dockerfile -t dedup .
fi

cd "$SCRIPT_DIR"

ID="$(docker create "$IMAGE")"

[ ! -d ../dedup_dists ] && mkdir ../dedup_dists

docker cp "$ID:/dedup_dists/" ../

docker rm -v "$ID"

echo "Distribution files created in: $(readlink -f "../dedup_dists")"






