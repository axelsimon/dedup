#!/bin/sh
set -e

IMAGE="dedup-tests"
SCRIPT="$(readlink -f "$0")"
SCRIPT_DIR="${SCRIPT%/*}"

cd "$SCRIPT_DIR/.."

if [ "$1" = "--force-build" ] || ! docker image inspect "dedup" 2>/dev/null 1>&2 ; then
    docker-compose -f docker/docker-compose.yml build
fi

if [ "$1" = "--force-build" ] || ! docker image inspect "$IMAGE" 2>/dev/null 1>&2 ; then
    docker build -f docker/tests.Dockerfile -t "$IMAGE" .
fi

# --privileged is required in order to use loop device inside container
if docker run --privileged "$IMAGE" .; then
    echo "All tests: OK"
else
    echo "Testing dedup failed!"
    exit 1
fi
