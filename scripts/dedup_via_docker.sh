#!/bin/sh
set -e

IMAGE="dedup"
SCRIPT="$(readlink -f "$0")"
SCRIPT_DIR="${SCRIPT%/*}"


if [ -z "$DEDUP_HOST_DIR" ] || [ ! -d "$DEDUP_HOST_DIR" ]; then
    echo "Environment variable DEDUP_HOST_DIR is not defined or does not point to an existing directory" >&2
    exit 1
fi

if [ "$1" = "--force-build" ] || ! docker image inspect "$IMAGE" 2>/dev/null 1>&2 ; then
    cd "$SCRIPT_DIR/.."
    docker build -f docker/Dockerfile -t dedup .
fi


docker run -v "$(readlink -f "$DEDUP_HOST_DIR"):/host_dir" "$IMAGE" "$@"