# Tests image
FROM dedup AS dedup-tests
LABEL org.opencontainers.image.authors="lasthere"

WORKDIR /dedup_tests

COPY ./scripts/test-dedup.sh ./

RUN apt-get -q update && \
        # Install build dependencies \
        DEBIAN_FRONTEND="noninteractive" apt-get -y --option Dpkg::Options::="--force-confnew" --no-install-recommends install \
        	udev \
                parted \
                fdisk \
                xfsprogs \
                btrfs-progs && \
        DEBIAN_FRONTEND="noninteractive" apt-get clean && \
        rm -r /var/lib/apt/lists /var/cache/apt

# Defaults to print-only /data mount point
CMD []

ENTRYPOINT ["/dedup_tests/test-dedup.sh"]

