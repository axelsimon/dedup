FROM debian:bullseye-slim AS dedup-builder
LABEL org.opencontainers.image.authors="lasthere"

#ARG DEDUP_RELEASE "v0.9.0"
ARG DEDUP_RELEASE "main"

# Prerequisites for Building
RUN apt-get -q update && \
        # Install build dependencies \
        DEBIAN_FRONTEND="noninteractive" apt-get -y --option Dpkg::Options::="--force-confnew" --no-install-recommends install \
                git \
                python3-all \
                python-is-python3 \
                python3-setuptools \
                build-essential \
                fakeroot \
                dh-python \
                python3-stdeb

# Optional Source location
COPY ./ /tmp/dedup/

# If empty then get source from gitlab
ENV LOCAL_SOURCE="./"

# Build
RUN cd /tmp && \
        if [ -z "$LOCAL_SOURCE" ]; then \
                git clone "https://gitlab.com/lasthere/dedup:${DEDUP_RELEASE}"; \
        fi; \
        cd dedup && \
        python setup.py sdist && \
        python setup.py bdist && \
        python setup.py --command-packages=stdeb.command bdist_deb && \
        # python setup.py --command-packages=stdeb.command debianize && \
        # find . -printf "%TY-%Tm-%Td %TH:%TM:%TS %M %7s\t%p\n" && \
        # Cleanup \
        DEBIAN_FRONTEND="noninteractive" apt-get -y autopurge \
        	git python3-all python3-setuptools build-essential fakeroot dh-python python3-stdeb && \
        DEBIAN_FRONTEND="noninteractive" apt-get clean && \
        rm -r /var/lib/apt/lists /var/cache/apt


# Runtime image
FROM debian:bullseye-slim AS dedup
LABEL org.opencontainers.image.authors="lasthere"

WORKDIR /dedup_dists

COPY --from=dedup-builder /tmp/dedup/deb_dist/*.deb \
			  /tmp/dedup/deb_dist/*.dsc \
			  /tmp/dedup/deb_dist/*.debian.tar.xz \
			  /tmp/dedup/dist/* \
			  ./

#RUN mkdir debian
#COPY --from=dedup-builder /tmp/dedup/debian/* ./debian/

RUN apt-get -q update && \
        # Install build dependencies \
        DEBIAN_FRONTEND="noninteractive" apt-get -y --option Dpkg::Options::="--force-confnew" --no-install-recommends install \
                python-is-python3 && \
	dpkg -i *.deb && \
        DEBIAN_FRONTEND="noninteractive" apt-get clean && \
        rm -r /var/lib/apt/lists /var/cache/apt

# BTRFS or XFS host directory on which to run/test dedup
VOLUME /host_dir

WORKDIR /host_dir

# Defaults to print-only /data mount point
CMD ["/host_dir", "--print-only", "all"]

ENTRYPOINT ["/usr/bin/dedup"]
