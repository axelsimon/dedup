
# DEDUP - Identical File Finder and Deduplication

Searches for identical files on large directory trees with multi millions of files consuming multi terabytes of 
data in an extremely efficient and fast way and by default also deduplicates those identical files via the Linux
system function [ioctl(FIDEDUPERANGE)](https://man.archlinux.org/man/ioctl_fideduperange.2.en).

**Deduplicating** identical files saves disk space by letting the files transparently share the same space on
disk without any side effects like symlinking or hard linking those files would have.
So that after this process, one can still modify the individual files without affecting the other identical files.
See ([COW](https://en.wikipedia.org/wiki/Copy-on-write)) for technical details.

If the **Deduplication** is run regularly, so that only a few GB of data has been changed, then this process may take
less than a minute per one terabyte of original (none-deduplicated) file sizes. See command line options for
further tweaks.

**NOTE**: currently only a limited set of file systems support the Linux function `ioctl(FIDEDUPERANGE)`. Especially 
the widely used `ext4` filesystem does **not** support this deduplication.  
According to [https://en.wikipedia.org/wiki/List_of_default_file_systems](https://en.wikipedia.org/wiki/List_of_default_file_systems),
the following Linux distributions are supported through their default filesystems:

- RHEL 7: XFS
- CentOS 7: XFS
- Fedora 22 Server: XFS
- OpenSUSE 42.1: BTRFS (for system), XFS (for home)
- RHEL 8: XFS
- Fedora 33: BTRFS
- *On all other Linux distributions a manuall partition formatting is required.*

Without a filesystem supporting **Deduplication** `dedup` can still be used as a still pretty fast 
*"find identical files"* tool. 

When looking at the performance numbers between `dedup` and other *"find same files"* tools (see below), then 
just this aspect already justifies in many cases to switch to one of the supported files systems like `XFS` or `BTRFS`.
`dedup` is currently the only known *"find same files"* tool with file level deduplication support where processing
time scales along the netto (deduplicated) size, while all other tools processing time scale with the brutto file sizes.



## Getting Started

### Run directly from Source

```sh
sudo apt-get install git
git clone https://gitlab.com/lasthere/dedup
cd dedup
# Run:
scripts/dedupe_via_source.sh
```

### Install

#### Python way to install:

```sh
sudo apt-get install python3-setuptools git
git clone https://gitlab.com/lasthere/dedup
cd dedup
sudo ./setup.py install
# Run:
dedup
```

#### Install by building Debian Package via Docker

```sh
git clone https://gitlab.com/lasthere/dedup
cd dedup
scripts/build-dists.sh
sudo dpkg -i dedup_dists/*.deb
# Run:
dedup
```

### Testing from inside Docker Container

```sh
git clone https://gitlab.com/lasthere/dedup
cd dedup
scripts/dedup_tests_via_docker.sh
```


### Run from inside Docker Container

This requires to set ahead the `DEDUP_HOST_DIR` environment variable pointing to a host directory which gets
mounted into the container. So that all container `dedup` commands execute relative to that directory.
Inside the container `DEDUP_HOST_DIR` is mounted to: `/host_dir`  
Example performing all test runs in `$HOME/tests` having `dir1` and `dir2` subdirectories:

```sh
sudo apt-get install git
git clone https://gitlab.com/lasthere/dedup
alias dedup="$(readlink -f dedup/scripts/dedup_via_docker.sh)"
# Run:
export DEDUP_HOST_DIR="$HOME/tests"
cd "$DEDUP_HOST_DIR"
dedup ./dir1 /host_dir/dir2 --print-only all   
```

## Usage

    usage: dedup [-h] [--dup-src DUP_SRC [DUP_SRC ...]] [--verbose] [--quiet] [--version] [--fail-always]
                 [--multi-threaded] [--use-reflink] [--no-partial-dedup] [--no-recursive] [--start-small]
                 [--size SIZE] [--mtime MTIME] [--match-re MATCH_RE] [--print-only {all,new,sh}] [--dry-run]
                 [--block-size BLOCK_SIZE]
                 DIRECS [DIRECS ...]
    
    Identical File Finder and Deduplication.
    
    Searches for identical files on large directory trees with multi millions of files consuming multi terabytes
    of data in an extremely efficient and fast way and by default also deduplicates those identical files via
    the Linux system function ioctl(FIDEDUPERANGE).
    
    NOTE: currently only a limited set of file systems support the Linux function `ioctl(FIDEDUPERANGE)`.
    Especially the widely used `ext4` filesystem does *not* support this deduplication.
    Without a filesystem supporting *Deduplication* `dedup` can still be used as a still pretty fast
    *"find identical files"* tool. See --print-only option.
    
    positional arguments:
      DIRECS                directories and/or files to lookup/search for processing and optional deduplication.
                            For deduplication, all given files and directories must be located on the same
                            filesystem. Mount points (to other filesystems) inside those directories are
                            silently skipped.
                            If not deduplicating (option '--print-only'), then different filesystems are
                            accepted,
                            but will reduce the overall lookup performance.
    
    optional arguments:
      -h, --help            show this help message and exit
      --dup-src DUP_SRC [DUP_SRC ...], -D DUP_SRC [DUP_SRC ...]
                            additional files and/or directories from where identical files are taken only as
                            source for deduplication, but no file from here gets deduplicated itself.
                            I.e.: all files here remain unchanged, but other files from DIRECS may deduplicate
                            such that they will share the same content with files found here.
                            Files found here may be concurrently modified by other processes. And the same
                            filesystem  requirements apply as in DIRECS.
                            Usage examples:
                            1.) speed up deduplication if those directories were deduplicated earlier and it is
                                known that there were no changes meanwhile. So files found withing DUP_SRC will
                                not be read at all, if only other files within DUP_SRC could be matching
                                candidates.
                            2.) when using '--use-reflink' all files in DIRECS must not be modified by other
                                processes during the deduplication processing.
                                In order to still support deduplicating those files with other directories that
                                may be concurrently modified, those other directories can be added here, but only
                                as deduplication source. (default: None)
      --verbose, -v         more detailed output (default: False)
      --quiet, -q           don't even output status information about current running deduping progress.
                            (default: False)
      --version, -V         output dedup version and exit
      --fail-always, -F     exit with an error (2) even for recoverable errors like File-Not-Found or Permission
                            errors. By default, those errors are handled such that the involved files are skipped
                            and only a warning message is output. (default: False)
      --multi-threaded, -M  use several threads for deduplication. Deduplication is mainly disk IO bound.
                            But on modern disks which support command queuing, this may still deliver faster
                            results. (default: False)
      --use-reflink, -l     By default, deduplication is done via system function ioctl(FIDEDUPERANGE). This
                            system function supplies fully transparent race free deduplication without any side
                            effects under any workload.
                            But as of Linux version 6.0 this is pretty slow. A faster approach which finally also
                            results in deduplicated files is by replacing identical files with new "reflink"
                            copied files (via ioctl_ficlone()).
                            This however can go wrong if other processes concurrently modify the files which get
                            replaced by the "reflink" copies.
                            It is highly advised to use this only if it is ensured that all files in given DIRECS
                            do not get modified by other processes during the deduplication process.
                             (default: False)
      --no-partial-dedup, -N
                            The system function ioctl(FIDEDUPERANGE), checks if source and destination content is
                            identical before performing the actual deduplication. But deduplication, happens in
                            ranges, where each range of a file is checked and deduplicated separately.
                            To speed up deduplication, the implementation by default, does not always check if
                            files are identical within all ranges, and passes the final full equal check to this
                            system function.
                            This then may result in having only the start of two files deduplicated up to the
                            point where the files differ.
                            This option forces to always check ahead that two files are entirely identical before
                            performing the system function ioctl(FIDEDUPERANGE).
                            NOTE: there is still the possibility that between this check and the deduplication,
                            another process modified one of the files, so that deduplication again will only
                            happen within equal ranges. I.e.: there still is no guarantee that only entire files
                            contents get deduplicated.
                            This does not apply when using: --use-reflink (default: False)
      --no-recursive, -n    By default all given directories are searched recursive (nested).
                            This option will only lookup files in those given directories, ignoring any sub
                            directory. (default: False)
      --start-small, -S     By default deduplication starts with the large files. This option makes it start with
                            small files first. (default: False)
      --size SIZE, -s SIZE  check only files with the given size. Format: [[+]<min_size>][-<max_size>]
                            Where <min_size> and <max_size> can be suffixed with upper or lower cased: K, M, G
                            and T as their corresponding powers of 1024.
                            Examples:
                            -s +1K         - 1024 bytes or more
                            --size=-100m   - less than 100 mega bytes
                            -s 1000-1T     - 1000 bytes until at most one terrabyte.
                            -s 338-338     - only files with exactly 338 bytes
                            Note: if the value starts with '-', then the long option passing with '=' must be
                            used, in order to prevent confusion with other dedup options.
                             (default: )
      --mtime MTIME, -t MTIME
                            check only files with the given modification date. Min/max ranges work like in
                            '--size'.
                            The dates can be expressed in 's'=seconds, 'm'=minutes, 'h'=hours, 'd'=days suffixed
                            numbers as back from now, or by an ISO formated date/time. Also spaces are required
                            for the '-' between separator. So quoting is necessary. Examples:
                            -t "2019-03 - 23:00"         - from march 1st 2019 until 23:00 o'clock today
                            --mtime="-2022-12-12T08:00"  - all before december 12th 2022 at 8:00 o'clock
                            --mtime=-100d                - all modified before 100 days back from now
                            -t +100d                     - all modified since 100 days back from now (default: )
      --match-re MATCH_RE, -m MATCH_RE
                            REGEXP pattern which must match against the plain file basename (no path).
                            E.g.: "vm-[0-9].*\.qcow2|test.img" matches file names starting with "vm-<digit>" and
                            ending with '.qcow2' or file 'test.img' (default: )
      --print-only {all,new,sh}, -p {all,new,sh}
                            Only print out identical files. One file per line and empty line between none
                            identical files. No changes will be actually made on the files.
                            All file names are prefixed with one of:
                            '+' file to be deduped which has currently no shares with any other identical file.
                            '*' first file of a group of files sharing same space, but still needs to be
                            re-deduped to another identical file ('~') having more shares.
                            '=' files sharing data with above '*' prefixed one, but still need to be re-deduped
                                to another file having more shares ('~').
                            '~' first file of a group of identical files, and picked as the source for
                                deduplication of all identical files. But no action is needed on this file.
                            '-' files already deduped to above '~' prefixed file. No action is needed on this
                                file.
                            To summarize: '+', '*' and '=' files need to be deduplicated, but disk space is only
                            saved for '+' and '*' files.
                            Options:
                            'all'   - prints out all of above prefixed files
                            'new'   - does not print out the '-' prefixed files and the '~' ones if no other not
                                      yet shared file is identical to it. I.e.: 'new' prints out only those files
                                      involved in pending deduplication.
                            'sh'    - print out the result as a shell script which can then be easily adjusted to
                                      perform any desired task on those identical files.
                            NOTE: files in DUP_SRC are only listed (and accounted) if they are picked as
                                  deduplication sources.
                             (default: )
      --dry-run, -d         do not perform any deduplication, resulting in no filesystem modifications.
                            Just show what would be performed. (default: False)
      --block-size BLOCK_SIZE, -b BLOCK_SIZE
                            the block size used by the filesystem. Larger multiples of that size are also
                            allowed. The default should be good for most filesystems. (default: 4096)
    
## Status

Privately it was used productive for quite some time. Limited to only a few Linux distributions/versions and the
use cases were also mostly the same.  
But for public usage it is brand new.  
The main modifying function is [ioctl(FIDEDUPERANGE)](https://man.archlinux.org/man/ioctl_fideduperange.2.en), 
which according to its documentation should be completely safe, even when called wrong. But python as of version 3.10
does not have native `ioctl_fideduprange()` support, so that low level `ioctl()` has to emulate it. And that part is
critical. Especially when it is executed on not yet tested platforms other than the well tested x64 platform.

Therefor it is considered in Beta State.  

## RAM Usage

Processing needs around 100 MB RAM per one million files, depending on the concrete file names.  
If running out of memory, then one workaround is to run it several times with different `--min-size`/`--max-size`
option pairs.

## Performance

Performance tests were done on an actual (2022) H-CPU Laptop with XFS filesystem.
Each test is started with empty file system cache.

### Find same Files Comparison with other Tools

#### Many relative small files:

On external USB-SATA TLC SSD (540 MB/s, USB-3 Gen2/10 GBits).  

    Processed directories:           218,457
    Processed files:               1,736,143
    Processed files size:     32,570,420,978
    Already deduped files:         1,042,496
    Already deduped size:     14,213,966,378
    Dedupeable files:                      0
    Dedupeable size:                       0

**Result:**

- dedup v0.9.0: - real: `3m21.461s`, user: `0m29.631s`, sys: `0m32.355s`
- rmlint 2.9.0: - real: `6m14.999s`, user: `1m40.382s`, sys: `1m39.245s`
- jdupes 1.20.2: - real: `7m26.895s`, user: `0m17.775s`, sys: `1m19.907s`

#### Collection of Large Media Files

On PCIe-4 TLC SSD

    Processed directories:             1,143
    Processed files:                  13,408
    Processed files size:  1,194,328,032,877
    Already deduped files:             2,242
    Already deduped size:    215,600,926,864
    Dedupeable files:                      0
    Dedupeable size:                       0

**Result:**
 
- dedup v0.9.0: - real: `0m0.772s`, user: `0m0.171s`, sys: `0m0.088s`
- rmlint 2.9.0: - real: `0m1.413s`, user: `0m2.381s`, sys: `0m0.763s`
- jdupes 1.20.2: - real: `3m53.661s`, user: `0m30.723s`, sys: `2m7.461s`

#### Backup with one single Reflink Snapshot

On external USB-SATA TLC SSD (540 MB/s, USB-3 Gen2/10 GBits).

    Processed directories:            21,540
    Processed files:                 242,078
    Processed files size:     68,797,144,672
    Already deduped files:           151,984
    Already deduped size:     36,230,538,402
    Dedupeable files:                      0
    Dedupeable size:                       0

**Result:**

- dedup v0.9.0: - real: `0m27.798s`, user: `0m4.167s`, sys: `0m4.246s`
- rmlint 2.9.0: - real: `3m4.160s`, user: `1m27.159s`, sys: `0m37.431s`
- jdupes 1.20.2: - real: `3m45.928s`, user: `0m13.491s`, sys: `0m43.979s`

When having already a lot of deduplicated data, this is where performance of `dedup` really shines.

### Deduplicating on an external 4 TB USB-SATA TLC SSD (540 MB/s, USB-3 Gen2/10 GBits)

Typical Backup with many deduped files summing up to brutto more than 8 TB of file size. 

#### With option: --print-only

    $ dedup /evoata4T_back/backups/latest/ --dup-src /evoata4T_back/backups/older/ --print-only new
    11:56:39 starting collecting: /evoata4T_back/backups/latest
    11:57:54 currently collected 3,800,000 files
    11:57:54 starting collecting: /evoata4T_back/backups/older
    12:00:17 currently collected 11,400,000 files
    12:00:19 sorting 11,489,283 matched files by size. Total size: 8,256,545,356,029
    12:00:19 collected total size 8,256,545,356,029 from 11,489,283 files with 199,689 different sizes
    ...
    12:21:01 Done. Result:
    12:21:01 Processed directories:         1,297,441
    12:21:01 Processed files:              11,489,283
    12:21:01 Processed files size:  8,256,545,356,029
    12:21:01 Not readable files:                    0
    12:21:01 Already deduped files:         9,832,815
    12:21:01 Already deduped size:  5,902,599,959,861
    12:21:01 Dedupeable files:                 18,591
    12:21:01 Dedupeable size:               7,206,069
    12:21:01 Total process time:             00:11:03

#### Now Deduplicating

    $ dedup /evoata4T_back/backups/latest/ --dup-src /evoata4T_back/backups/older/
    12:22:57 starting collecting: /evoata4T_back/backups/latest
    12:24:10 currently collected 3,800,000 files
    12:24:11 starting collecting: /evoata4T_back/backups/older
    12:26:28 currently collected 11,400,000 files
    12:26:29 sorting 11,489,283 matched files by size. Total size: 8,256,545,356,029
    12:26:29 collected total size 8,256,545,356,029 from 11,489,283 files with 199,689 different sizes
    ...
    12:34:40 Done. Result:
    12:34:40 Processed directories:         1,297,441
    12:34:40 Processed files:              11,489,283
    12:34:40 Processed files size:  8,256,545,356,029
    12:34:40 Not readable files:                    0
    12:34:40 Already deduped files:         9,832,815
    12:34:40 Already deduped size:  5,902,599,959,861
    12:34:40 Deduped files:                    18,591
    12:34:40 Deduped size:                  7,206,069
    12:34:40 Total process time:             00:11:43

## TODO

- 

## FAQ

### - Why is this not in pip?
The plan is to get this quickly into the Linux distributions. And exposing his own filesystem to anything pulled from
`pip`, is not advisable.  
So as long as it is not found in the Linux distributions, one needs to check the code ahead anyway. And then there
is `scripts/dedup_via_source.sh` in the source which can be directly executed from there without having to install it.

### - Is there a Windows/MAC Version?
no.

### - Howto get `jdupes` compatible output?

```dedup <DIRECTORY> --print-only all --quiet | sed -e 's/^[*+=~-]//g'```


## Changelog

See: [Changelog](./Changelog.md)

## History

DEDUP was initially written as part of a simple backup solution using `rsync` into `.../latest` directory and
afterwards `"cp --reflink"` fron `.../latest` into a directory named by the actual backup date/time. Because such
solutions allow easy command line filtering/cleanup like:

```sh
    $ find . -size +100m --name "*.qcow2" -exec rm {} \;
```

More generally: most existing backup solutions lack the possibility to remove only some files from older backup snapshots
using standard command line tools.

The above scenario when using `"rsync --inplace"` ensures already very good and efficient deduplication. But sooner or
later comes the point when one wants to check/see if there is not even more space to deduplicate across older backup
directories.

Naively trying out the existing deduplication tools in a *"deduplicate all"* mode resulted in having to run each tool
overnight to finally figure out that the tool was not meant for such scenarios. Depending on the tool one could choose
between:
1. Out of Memory
2. Out of Disk Space
3. Core Dump
4. Still processing with no indication how long it might take to finish

That was the reason to create `dedup`.

## Related

- [jdupes](https://github.com/jbruchon/jdupes)  - find identical files with --dedupe option
- [rmlint](https://github.com/sahib/rmlint)  - find identical files with 'btrfs' support
- [duperemove](https://github.com/markfasheh/duperemove)  - block level deduplication
- [btrfs](https://btrfs.wiki.kernel.org/index.php/Deduplication)  - BTRFS wiki about deduplication 

## License

GNU Affero General Public License (GNU AGPLv3)

## Disclaimer

**USE AT YOUR OWN RISK:** No guarantee for correct behavior of this software as intended and/or described here.
Especially: this is a low level tool performing heavy and pretty uncommon disk operations, which can result in complete 
data loss.

